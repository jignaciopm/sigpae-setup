# sigpae-setup

Un shell script que facilita la instalación de SIGPAE migrado a Python 3 y despliegue en Apache 2.

## Uso

```bash
$ sudo chmod +x sigpae-setup-options-unix_v6.0.2.sh
$ ./sigpae-setup-options-unix_v6.0.2.sh [-hrbwcalqetsv] [-p CLAVE] [-d HOST]
```

### Parametros 

```bash 
-p CLAVE  
```
Clave usuario sigpae DB, por defecto '123123'

```bash
-d HOST [ip:port]
```
Host ip:port para configurar el archivo 'appconfig.ini', por defecto 'localhost:8080'

### Opciones

```-r```    Resetea las bases de datos de SIGPAE y se limpia la carpeta 'databases'

```-b```    Instala las bases de datos de 'SIGPAE' y 'SIGPAE_WS', se limpian la carpetas 'databases'

```-w```    Reescribe los archivos appconfig.ini de 'SIGPAE' y 'SIGPAE_WS' usando -d $host y -p $password, 
	  si no se pasan se usan los valores por defecto

```-c```    Clonar los repositorios 'SIGPAE' y 'SIGPAE_WS'

```-a```   Instala apache2 con mod_wsgi_py3 y certificados SSL

```-l```    Instala librerias necesarias para SIGPAE, para mas informacion leer README

```-q```    Instala requerimientos o paquetes necesarias para SIGPAE, para mas informacion leer README

```-e```    Instala entorno de desarrollo y crea las bases de datos para SIGPAE_DEVELOPMENT

```-t```    Instala las bases de datos para SIGPAE_DEVELOPMENT

```-s```    Permisos recursivos para la carpeta $path_web2py

```-v``` 	  Version y fallas del script

## Recomendaciones
Antes de iniciar cualquier instalación, verifique la versión actual del shell script y las fallas que el mismo describa. 
Al momento de usar este archivo para SO no descritos en el mismo, es usted responsable y encargado de actualizar el codigo para reportar lo que suceda.

## Desarrollador
- [José Ignacio Palma](https://github.com/jignaciopm)

## Última Actualización
- 01 de Febrero de 2019.