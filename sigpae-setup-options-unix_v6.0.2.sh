#!/bin/sh

# Usage info
show_help() {
cat << EOF
Uso: 
    ${0##*/} [-hrbwcalqetsv] [-p CLAVE] [-d HOST]

Parametros:
    -p CLAVE            Clave usuario sigpae DB, por defecto '$password'

    -d HOST [ip:port]   Host ip:port para configurar el archivo 'appconfig.ini', por defecto '$host'

Opciones:
    -r    Resetea las bases de datos de SIGPAE y se limpia la carpeta 'databases'

    -b    Instala las bases de datos de 'SIGPAE' y 'SIGPAE_WS', se limpian la carpetas 'databases'

    -w    Reescribe los archivos appconfig.ini de 'SIGPAE' y 'SIGPAE_WS' usando -d $host y -p $password, 
    	  si no se pasan se usan los valores por defecto

    -c    Clonar los repositorios 'SIGPAE' y 'SIGPAE_WS'

    -a    Instala apache2 con mod_wsgi_py3 y certificados SSL

    -l    Instala librerias necesarias para SIGPAE, para mas informacion leer README

    -q    Instala requerimientos o paquetes necesarias para SIGPAE, para mas informacion leer README

    -e    Instala entorno de desarrollo y crea las bases de datos para SIGPAE_DEVELOPMENT

    -t    Instala las bases de datos para SIGPAE_DEVELOPMENT

    -s    Permisos recursivos para la carpeta $path_web2py

    -v 	  Version y fallas del script

Ayuda:
    Instala SIGPAE y SIGPAE-WebServices en esta maquina.
EOF
}

v_fallas() {
echo "Version 6.0.2"
echo "############################### FALLAS ################################
SO: 
    (*) Ubuntu Desktop 14 (x32) 
        DETALLE:
            {No se instalan las librerias correctamente}
            -psycopg2 {Falla por tener postgresql 9.3, se soluciono 
                        instalando libpq-dev y luego psycopg2}
        ESTADO:
            No se ha encontrado solucion para poner en funcionamiento
#######################################################################"

}

readme() {
echo "Esta a punto de instalar SIGPAE y SIGPAE-WebServices en esta maquina:

############################# HECHO PARA ##############################
SO: 
    Ubuntu Server 16 (x64), Ubuntu Desktop 16 (x64)
APPs:
    Web2Py, Python3.5, Apache2

(*) Si usted prueba este script en otras versiones, 
    otros SO o incluso para otras APPs, anotelas en esta seccion.
(-v) Utilice la opcion -v para listar las fallas con este script
	 y su version actual
#######################################################################

1) Instalacion de Python3, Pip, wget, zip y unzip
2) Despliegue en produccion en un solo paso Web2Py, MOD_WSGI: Python3 Apache2
3) Clonar repositorios SIGPAE_WS y SIGPAE
4) Dar permisos a $path_web2py
5) Intalacion de librerias requeridas para el 
   correcto funcionamiento de SIGPAE
6) Creacion y restauracion de base de datos
7) Reescribir los archivos 'appconfig.ini' para SIGPAE y SIGPAE_WS

(*) La instalacion se hara con el host=$host y password=$password
(*) La instalacion se hara en la ruta $path_web2py

############################ SEP-DIC 2018 #############################
Creado por: 
    Jose Ignacio Palma 13-11044
    Daniel Rodriguez 07-
Reportar error:
    jignaciopm13@gmail.com
#######################################################################


Presione una tecla para continuar o [ctrl+C] para abortar..."

read CONFIRM
}

first_step() {
    apt-get update
    echo "
    Instalacion de Python3 y Pip3"
    echo "==============================================="
    apt-get -y install python3
    apt-get -y install python3-pip

    echo "
    Instalacion de wget"
    echo "==============================================="
    apt-get -y install wget

    echo "
    Instalacion de git"
    echo "==============================================="
    apt-get -y install git

    echo "
    Instalacion de zip y unzip"
    echo "==============================================="
    apt-get -y install zip unzip
}

install_web2py_apache2_python3() {
    echo ""
    echo "Despliegue en produccion en un solo paso Web2Py"
    echo "***********************************************"
    echo "****************** Web2Py *********************"
    echo "***********************************************"
    echo "Web2Py: installing useful packages"
    echo "==============================================="
    apt-get -y install ssh
    apt-get -y install tar
    apt-get -y install openssh-server
    apt-get -y install build-essential
    apt-get -y install ipython
    apt-get -y install python3-dev
    apt-get -y install postgresql
    apt-get -y install apache2
    apt-get -y install libapache2-mod-wsgi-py3
    apt-get -y install postfix
    apt-get -y install mercurial
    /etc/init.d/postgresql restart

    echo "Web2Py: downloading, installing and starting web2py"
    echo "==============================================="
    cd /home
    mkdir www-data
    cd www-data
    rm web2py_src.zip*
    wget http://web2py.com/examples/static/web2py_src.zip
    unzip web2py_src.zip
    mv web2py/handlers/wsgihandler.py web2py/wsgihandler.py
    chown -R www-data:www-data web2py

    echo "Web2Py: setting up apache modules"
    echo "==============================================="
    a2enmod ssl
    a2enmod proxy
    a2enmod proxy_http
    a2enmod headers
    a2enmod expires
    a2enmod wsgi
    a2enmod rewrite  # for 14.04
    mkdir /etc/apache2/ssl

    echo "Web2Py: (SSL) creating a self signed certificate"
    echo "==============================================="
    openssl genrsa 1024 > /etc/apache2/ssl/self_signed.key
    chmod 400 /etc/apache2/ssl/self_signed.key
    openssl req -new -x509 -nodes -sha1 -days 365 -key /etc/apache2/ssl/self_signed.key > /etc/apache2/ssl/self_signed.cert
    openssl x509 -noout -fingerprint -text < /etc/apache2/ssl/self_signed.cert > /etc/apache2/ssl/self_signed.info

    echo "Web2Py: (Apache2-conf) rewriting your apache 
          config file to use mod_wsgi"
    echo "==============================================="
echo '
WSGIDaemonProcess web2py user=www-data group=www-data processes=1 threads=1

<VirtualHost *:80>

  RewriteEngine On
  RewriteCond %{HTTPS} !=on
  RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R,L]

  CustomLog /var/log/apache2/access.log common
  ErrorLog /var/log/apache2/error.log
</VirtualHost>

<VirtualHost *:443>
  SSLEngine on
  SSLCertificateFile /etc/apache2/ssl/self_signed.cert
  SSLCertificateKeyFile /etc/apache2/ssl/self_signed.key

  WSGIProcessGroup web2py
  WSGIScriptAlias / /home/www-data/web2py/wsgihandler.py
  WSGIPassAuthorization On

  <Directory /home/www-data/web2py>
    AllowOverride None
    Require all denied
    <Files wsgihandler.py>
      Require all granted
    </Files>
  </Directory>

  AliasMatch ^/([^/]+)/static/(?:_[\d]+.[\d]+.[\d]+/)?(.*) \
        /home/www-data/web2py/applications/$1/static/$2

  <Directory /home/www-data/web2py/applications/*/static/>
    Options -Indexes
    ExpiresActive On
    ExpiresDefault "access plus 1 hour"
    Require all granted
  </Directory>

  CustomLog /var/log/apache2/ssl-access.log common
  ErrorLog /var/log/apache2/error.log
</VirtualHost>
' > /etc/apache2/sites-available/default.conf  # FOR 14.04

    sudo rm /etc/apache2/sites-enabled/*    # FOR 14.04
    sudo a2ensite default                   # FOR 14.04

    echo "Web2Py: restarting apache"
    echo "==============================================="

    /etc/init.d/apache2 restart
    cd /home/www-data/web2py
    sudo -u www-data python3 -c "from gluon.widget import console; console();"

    echo "Web2Py: indique la clave para el admin de Web2Py"
    echo "==============================================="
    sudo -u www-data python3 -c "from gluon.main import save_password; save_password(input('admin password: '),443)"
    echo "***********************************************"
    echo "****************** Web2Py *********************"
    echo "***********************************************"
}

clone_sigpae_ws() {
    cd "$path_web2py"
    echo ""
    echo "GIT CLONE: SIGPAE_WS"
    echo "Introduzca sus credenciales en 'gitlab.com';"
    echo "usted debe tener permisos previos a estos repositorios"
    echo "==============================================="
    git clone https://gitlab.com/sigpae/SIGPAE_WS.git
    cd SIGPAE_WS
    git fetch
    git checkout migracion
    git pull
}

clone_sigpae() {
    cd "$path_web2py"
    echo ""
    echo "GIT CLONE: SIGPAE_MIGRACION"
    echo "Introduzca sus credenciales en 'gitlab.com';"
    echo "usted debe tener permisos previos a estos repositorios"
    echo "==============================================="
    git clone https://gitlab.com/regiosen/SIGPAE_MIGRACION.git
    mv "$path_web2py"/SIGPAE_MIGRACION "$path_sigpae"
    rm "$path_sigpae"/databases/*
}

clone_sigpae_development() {
    cd "$path_web2py"
    echo ""
    echo "GIT CLONE: SIGPAE_MIGRACION (Desarrollo)"
    echo "Introduzca sus credenciales en 'gitlab.com';"
    echo "usted debe tener permisos previos a estos repositorios"
    echo "==============================================="
    git clone https://gitlab.com/regiosen/SIGPAE_MIGRACION.git
    mv "$path_web2py"/SIGPAE_MIGRACION "$path_sigpae_dev"
    rm "$path_sigpae_dev"/databases/*
}

permit_www_data () {
    echo ""
    echo "Permisos para www-data"
    echo "==============================================="
    chown -R www-data:www-data "$path_web2py"
}

install_libraries() {
    echo ""
    echo "Librerias para conexion con el CAS"
    echo "==============================================="
    apt-get install -y libsasl2-dev python-dev libldap2-dev libssl-dev ldap-utils

    echo ""
    echo "Librerias para la lectura de PDF"
    echo "==============================================="
    apt-get install -y xpdf
    apt-get install -y libmagickwand-dev
    apt-get install -y tesseract-ocr libtesseract-dev libleptonica-dev
    apt-get install -y tesseract-ocr-eng tesseract-ocr-spa

    echo ""
    echo "Librerias para el funcionamiento de citeproc"
    echo "==============================================="
    apt-get install -y libxslt1.1 libxslt1-dev libxml2-dev libxml2
}

install_requeriments() {
    echo ""
    echo "Requerimientos para el funcionamiento de SIGPAE"
    echo "==============================================="
    cd "$path_sigpae"
    pip3 install -r requirements.txt

    echo ""
    echo "Otras librerias requeridas"
    echo "==============================================="
    pip3 install beautifulsoup4 
    pip3 install chardet 
    pip3 install citeproc-py 
    pip3 install psycopg2-binary 
    pip3 install idna 
    pip3 install latexcodec 
    pip3 install lxml 
    pip3 install Pillow 
    pip3 install pyasn1 
    pip3 install pybtex 
    pip3 install pymarc 
    pip3 install pyocr 
    pip3 install PyYAML 
    pip3 install six 
    pip3 install Unidecode 
    pip3 install Wand 
    pip3 install simplejson
    pip3 install matplotlib
    pip3 install reportlab

    # Para evitar inconsistencias se desinstala
    # ldap3 previos
    apt-get remove -y python3-ldap3
    apt-get purge -y python3-ldap3
    apt-get autoremove -y python3-ldap3
    pip3 install ldap3

    # Para evitar error de certificacion SSL, 
    # se debe instalar estas versiones de las
    # siguientes librerias
    pip3 install certifi==2018.8.24
    pip3 install requests==2.19.1
    pip3 install urllib3==1.23
}

create_user_database() {
    echo ""
    echo "Creacion del usuario SIGPAE:"
    echo "Si el usuario existe, se indica error pero continua..."
    echo "==============================================="
    printf "CREATE USER sigpae WITH PASSWORD '$password';" > user_sigpae.sql
    sudo -u postgres psql -f user_sigpae.sql
    rm user_sigpae.sql
}

create_database_sigpae() {
    echo ""
    echo "Creacion de bases de datos para SIGPAE:"
    echo "Se limpiara la carpeta ../SIGPAE/databases"
    echo "==============================================="
    rm "$path_sigpae"/databases/*
    echo "Deteniendo apache2 por seguridad..."
    service apache2 stop
    printf "DROP DATABASE IF EXISTS newsigpae; CREATE DATABASE newsigpae WITH OWNER sigpae;
    DROP DATABASE IF EXISTS newsigpae_test; CREATE DATABASE newsigpae_test WITH OWNER sigpae;
    DROP DATABASE IF EXISTS newsigpaemigration; CREATE DATABASE newsigpaemigration WITH OWNER sigpae;" > sigpae.sql
    sudo -u postgres psql -f sigpae.sql
    rm sigpae.sql
    service apache2 start
    echo "Encendiendo apache2..."
}

create_database_sigpae_ws() {
    echo ""
    echo "Creacion de base de datos 'dacepregrado' para SIGPAE_WS:"
    echo "Se limpiara la carpeta ../SIGPAE_WS/databases"
    echo "==============================================="
    rm "$path_sigpae_ws"/databases/*
    echo "Deteniendo apache2 por seguridad..."
    service apache2 stop
    printf "DROP DATABASE IF EXISTS dacepregrado; CREATE DATABASE dacepregrado WITH OWNER sigpae;" > sigpae_ws.sql
    sudo -u postgres psql -f sigpae_ws.sql
    rm sigpae_ws.sql

    echo "
    Restaurando base de datos 'dacepregrado':"
    echo "==============================================="
    sudo -u postgres psql --set ON_ERROR_STOP=on dacepregrado < "$path_sigpae_ws"/backup_file;
    service apache2 start
    echo "Encendiendo apache2..."
}

create_databases() {
    create_user_database
    create_database_sigpae
    create_database_sigpae_ws
}

create_database_sigpae_development() {
    echo ""
    echo "Creacion de bases de datos para SIGPAE_DEVELOPMENT:"
    echo "Se limpiara la carpeta ../desarrollo/databases"
    echo "==============================================="
    rm "$path_sigpae_dev"/databases/*
    echo "Deteniendo apache2 por seguridad..."
    service apache2 stop
    printf "DROP DATABASE IF EXISTS newsigpae_development; CREATE DATABASE newsigpae_development WITH OWNER sigpae;
    DROP DATABASE IF EXISTS newsigpae_development_test; CREATE DATABASE newsigpae_development_test WITH OWNER sigpae;
    DROP DATABASE IF EXISTS newsigpaemigration_development; CREATE DATABASE newsigpaemigration_development WITH OWNER sigpae;" > sigpae_development.sql
    sudo -u postgres psql -f sigpae_development.sql
    rm sigpae_development.sql
    service apache2 start
    echo "Encendiendo apache2..."
}

write_appconfig_sigpae() {
    echo "Reescribiendo ../SIGPAE/private/appconfig.ini"
    echo "
; App configuration
[app]
name        = SIGPAE
title       = SIGPAE        
description = a cool new app
keywords    = web2py, python, framework
generator   = Web2py Web Framework
production  = false
toolbar     = false

; Host configuration
[host]
names = localhost:*, 127.0.0.1:*, *:*, *
hostname_url = http://$host/
login_method = local,
return_url = http://$host/SIGPAE/default/login_cas
cas_login_url = https://secure.dst.usb.ve/login?service=
cas_verify_url = https://secure.dst.usb.ve/validate?ticket=
cas_logout_url = http://secure.dst.usb.ve/logout

; configuracion del webservice
[dace_ws]
url = http://$host/SIGPAE_WS/default/webservices

[loc]
service_url = http://lx2.loc.gov:210/LCDB?

; db configuration
[db]
uri       = postgres://sigpae:$password@localhost/newsigpaemigration
test_uri  = postgres://sigpae:$password@localhost/newsigpae_test
migrate   = true
pool_size = 10  

; smtp address and credentials
[smtp]
server = smtp.gmail.com:587
sender = noreplysigpaeusb@gmail.com
login  = noreplysigpaeusb@gmail.com:LXHyCmFD9rQPCqC
tls    = true
ssl    = true

[scheduler]
enabled   = false
heartbeat = 1

[google]
analytics_id =

[forms]
formstyle = bootstrap3_inline" > "$path_sigpae"/private/appconfig.ini
}

write_appconfig_sigpae_ws()
{
    echo "Reescribiendo ../SIGPAE_WS/private/appconfig.ini"
    echo "
; App configuration
[app]
name        = SIGPAE_WS
title       = SIGPAE_WS

; Host configuration
[host]
names = localhost:*, 127.0.0.1:*, *:*, *

; db configuration
[db]
uri       = postgres://sigpae:$password@localhost/dacepregrado
migrate   = true
pool_size = 10
; ignored for sqlite

; smtp address and credentials
[smtp]
server = smtp.gmail.com:587
sender = noreplysigpaeusb@gmail.com
login  = noreplysigpaeusb@gmail.com:LXHyCmFD9rQPCqC
tls    = true
ssl    = true

; form styling
[forms]
formstyle = bootstrap3_inline" > "$path_sigpae_ws"/private/appconfig.ini
}

write_appconfig_sigpae_development() {
    echo "Reescribiendo ../desarrollo/private/appconfig.ini"
    echo "
; App configuration
[app]
name        = SIGPAE Desarrollo
title       = SIGPAE Desarrollo      
description = a cool new app
keywords    = web2py, python, framework
generator   = Web2py Web Framework
production  = false
toolbar     = false

; Host configuration
[host]
names = localhost:*, 127.0.0.1:*, *:*, *
hostname_url = http://$host/
login_method = local,
return_url = http://$host/desarrollo/default/login_cas
cas_login_url = https://secure.dst.usb.ve/login?service=
cas_verify_url = https://secure.dst.usb.ve/validate?ticket=
cas_logout_url = http://secure.dst.usb.ve/logout

; configuracion del webservice
[dace_ws]
url = http://$host/SIGPAE_WS/default/webservices

[loc]
service_url = http://lx2.loc.gov:210/LCDB?

; db configuration
[db]
uri       = postgres://sigpae:$password@localhost/newsigpaemigration_development
test_uri  = postgres://sigpae:$password@localhost/newsigpae_development_test
migrate   = true
pool_size = 10  

; smtp address and credentials
[smtp]
server = smtp.gmail.com:587
sender = noreplysigpaeusb@gmail.com
login  = noreplysigpaeusb@gmail.com:LXHyCmFD9rQPCqC
tls    = true
ssl    = true

[scheduler]
enabled   = false
heartbeat = 1

[google]
analytics_id =

[forms]
formstyle = bootstrap3_inline" > "$path_sigpae_dev"/private/appconfig.ini
}

end() {
    echo "LISTO! 
    Se han reescrito los archivos:
        ../SIGPAE/private/appconfig.ini y ../SIGPAE/private/appconfig.ini,
    Con las rutas, claves y usuarios de acuerdo al entorno.
    rutas: $host, usuario: sigpae, clave: $password
    ---------------------------------------------------------
    Segun su configuracion, los aplicativos son accesbiles 
    desde:

    SIGPAE:     https://$host/SIGPAE
    SIGPAE_WS:  https://$host/SIGPAE_WS
    ---------------------------------------------------------
    "
}

install_full() {
    readme
    first_step
    # OPCION -a
    install_web2py_apache2_python3
    # OPCION -c
    clone_sigpae_ws
    clone_sigpae
    # OPCION -s
    permit_www_data
    # OPCION -l
    install_libraries
    # OPCION -q
    install_requeriments
    # OPCION -b
    create_databases
    # OPCION -w
    write_appconfig_sigpae
    write_appconfig_sigpae_ws
    end
}

# Inicializamos nuestras propias variables:
#ip="$(ifconfig | grep -A 1 'enp0s3' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)"
host="localhost:8000"
password="123123"
path_web2py="/home/www-data/web2py/applications"
path_sigpae="$path_web2py/SIGPAE"
path_sigpae_ws="$path_web2py/SIGPAE_WS"
path_sigpae_dev="$path_web2py/desarrollo"
only_write_appconfig="NO"
only_create_database_sigpae="NO"
only_create_databases="NO"
only_clone_repository="NO"
only_apache="NO"
only_libraries="NO"
only_requeriments="NO"
only_permit="NO"
only_developmentenv="NO"
only_databases_developmentenv="NO"
OPTIND=1
# Es necesario restablecer OPTIND si anteriormente se uso getopts en el script.
# Es una buena idea hacer que OPTIND sea local si procesa opciones en una funcion.

while getopts hrbwcalqetsvd:p: opt; do
    case $opt in
        h)
            show_help
            exit 0
            ;;
        r)  only_create_database_sigpae="SI"
            ;;
        b)  only_create_databases="SI"
            ;;
        w)  only_write_appconfig="SI"
            ;;
        c)  only_clone_repository="SI"
            ;;
        a)  only_apache="SI"
            ;;
        l)  only_libraries="SI"
            ;;
        q)  only_requeriments="SI"
            ;;
        e)  only_developmentenv="SI"
            ;;
        t)  only_databases_developmentenv="SI"
            ;;
        s)  only_permit="SI"
            ;;
        v)  v_fallas
			exit 0
            ;;
        d)  host=$OPTARG
            ;;
        p)  password=$OPTARG
            ;;
        *)
            show_help >&2
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))"   # Discard the options and sentinel --

# OPCION -a
###################################################
# SE INSTALA WEB2PY CON APACHE2 Y PYTHON3 SI 
# NO ESTA INSTALADO, EN CASO DE QUERER REINSTALAR
# BORRE LA CARPETA '/home/www-data'
if test $only_apache = "SI"; then
    if [ ! -d "$path_web2py" ]; then
        install_web2py_apache2_python3
    else
        echo "Ya ha sido instalado web2py, borre la carpeta '/home/www-data' para reinstalar..."
    fi
fi

# OPCION -c
###################################################
# SE CLONAN LOS REPOSITORIOS 'SIGPAE' Y 'SIGPAE_WS'
# SIEMPRE Y CUANDO YA SE HAYA INSTALADO WEB2PY
# Y LAS APLICACIONES NO EXISTAN
if test $only_clone_repository = "SI"; then
    if [ -d "$path_web2py" ]; then
        if [ ! -d "$path_sigpae_ws" ]; then
            clone_sigpae_ws
            if [ ! -d "$path_sigpae" ]; then
                clone_sigpae
            else
                echo "Ya existe la aplicacion SIGPAE en Web2Py, si quiere volver a clonar borre esta aplicacion..."
            fi
        else
            echo "Ya existe la aplicacion SIGPAE_WS en Web2Py, si quiere volver a clonar borre esta aplicacion..."
        fi
    else
        echo "No ha sido instalado web2py, utilice -a para instalarlo junto apache2..."
    fi
fi

# OPCION -w
###################################################
# SE REESCRIBE EL 'appconfig.ini' DE 'SIGPAE' Y 'SIGPAE_WS'
if test $only_write_appconfig = "SI"; then
    if [ -d "$path_web2py" ]; then
        if [ -d "$path_sigpae" ]; then
            write_appconfig_sigpae
        else
            echo "No existe la aplicacion SIGPAE en Web2Py, utilice -c para clonar el repositorio..."
        fi

        if [ -d "$path_sigpae_ws" ]; then
            write_appconfig_sigpae_ws
        else
            echo "No existe la aplicacion SIGPAE_WS en Web2Py, utilice -c para clonar el repositorio..."
        fi

        if [ -d "$path_sigpae_dev" ]; then
            write_appconfig_sigpae_development
        else
            echo "No existe la aplicacion SIGPAE_DEVELOPMENT en Web2Py, utilice -e para instalar..."
        fi
    else
        echo "No ha sido instalado web2py, utilice -a para instalarlo junto apache2..."
    fi
fi

# NO SE PUEDEN EJECUTAR LAS OPCIONES -r Y -b JUNTAS
if test $only_create_database_sigpae = "SI"; then
    if test $only_create_databases = "SI"; then
        echo "No puede mezclar las opciones -r y -b, intente de nuevo..."
        exit 0
    fi
fi

# OPCION -r
###################################################
# DROPEA Y CREA DE NUEVO LAS BASES DE DATOS PARA 'SIGPAE',
# SE LIMPIA ADEMAS LA CARPETA 'databases'
if test $only_create_database_sigpae = "SI"; then
    create_database_sigpae
fi

# OPCION -b
###################################################
# CREA Y RESTAURA LAS BASES DE DATOS PARA 'SIGPAE' Y 'SIGPAE_WS'
if test $only_create_databases = "SI"; then
    create_databases
fi

# OPCION -l
###################################################
# INSTALA LIBRERIAS PARA 'SIGPAE'
if test $only_libraries = "SI"; then
    install_libraries
fi

# OPCION -q
###################################################
# INSTALA LIBRERIAS PARA 'SIGPAE'
if test $only_requeriments = "SI"; then
    install_requeriments
fi

# OPCION -s
###################################################
# PERMISOS PARA LA RUTA '/home/www-data/web2py/applications'
if test $only_permit = "SI"; then
    if [ -d "$path_web2py" ]; then
        permit_www_data
    else
        echo "No ha sido instalado web2py, utilice -a para instalarlo junto apache2..."
    fi
fi

# OPCION -t
###################################################
# DROPEA Y CREA DE NUEVO LAS BASES DE DATOS PARA 'SIGPAE_DEVELOPMENT',
# SE LIMPIA ADEMAS LA CARPETA 'databases'
if test $only_databases_developmentenv = "SI"; then
    create_database_sigpae_development
fi

# OPCION -e
###################################################
# INSTALA EN 'applications' EL ENTORNO DE DESARROLLO PARA SIGPAE
# DROPEA Y CREA DE NUEVO LAS BASES DE DATOS PARA 'SIGPAE_DEVELOPMENT',
# SE LIMPIA ADEMAS LA CARPETA 'databases'
if test $only_developmentenv = "SI"; then
    if [ -d "$path_web2py" ]; then
        if [ ! -d "$path_sigpae_dev" ]; then
            clone_sigpae_development
            create_database_sigpae_development
            write_appconfig_sigpae_development
        else
            echo "Ya ha sido instalado el entorno de desarrollo SIGPAE, borre la carpeta '$path_sigpae_dev' para reinstalar..."
        fi
    else
        echo "No ha sido instalado web2py, utilice -a para instalarlo junto apache2..."
    fi
fi

# INSTALACION COMPLETA SI NINGUNA OPCION ESTA ACTIVA
if test $only_write_appconfig = "NO"; then
    if test $only_create_database_sigpae = "NO"; then
        if test $only_create_databases = "NO"; then
            if test $only_clone_repository = "NO"; then
                if test $only_apache = "NO"; then
                    if test $only_libraries = "NO"; then
                        if test $only_requeriments = "NO"; then
                            if test $only_permit = "NO"; then
                                if test $only_developmentenv = "NO"; then
                                    if test $only_databases_developmentenv = "NO"; then 
                                        install_full
                                    fi
                                fi
                            fi
                        fi
                    fi
                fi 
            fi
        fi
    fi
fi
# End of file


